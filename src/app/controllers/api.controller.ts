import { Context, Get, HttpResponseOK } from '@foal/core';

export class ApiController {

  @Get('/')
  index(ctx: Context) {
    return new HttpResponseOK('Hello world!');
  }

  @Get('/appname/banner')
  banner(ctx: Context) {
    return new HttpResponseOK('im code !!!!');
  }

}
